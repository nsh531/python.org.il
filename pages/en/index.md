---
title: Python in Israel
---

# Python in Israel

## About

This is a community homepage for users of the [Python](http://www.python.org/) programming language in Israel.


## Links

* [PyWeb-IL](/en/pyweb)  has regular meetings See the [PyWeb-IL on Meetup](https://www.meetup.com/PyWeb-IL/).
* [PyData Tel Aviv in Meetup](https://www.meetup.com/pydata-tel-aviv/)
* [PyCon IL](https://pycon.org.il/) - annual Python conerence in Israel.
* [PyData Tel Aviv 2023](https://pydata.org/telaviv2023/).
* [Pyweb-IL mailing list](http://groups.google.com/group/pyweb-il).

* Facebook
    * [Python Israel](https://www.facebook.com/groups/pythonisraelgroup/)

* LinkedIn
    * [Python Developers Israel](https://www.linkedin.com/groups/8365068/)
    * [Python in Israel](https://www.linkedin.com/groups/3865694/)
    * [Python masters israel](https://www.linkedin.com/groups/13520250/)
    * [Israel Python Leaders ](https://www.linkedin.com/groups/9073989/)

* Telegram
    * [Python in Hebrew](https://t.me/PythonIsrael)

* [Older static site](/old/) (some links may be broken).


